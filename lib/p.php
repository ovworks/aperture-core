<?php

/**
 * $_POST
 */
class P {

    static function getArray($variable_name) {
        if (!empty($_POST[$variable_name])) {
            $get = $_POST[$variable_name];
            if (is_array($get)) {
                return $get;
            }
        }
    }

    static function get($variable_name) {
        return filter_input(INPUT_POST, $variable_name);
    }

    static function varchar($variable_name) {
        return filter_input(INPUT_POST, $variable_name);
    }

}

class Request {

    static function get($variable_name) {
        return !empty($_REQUEST[$variable_name]) ? $_REQUEST[$variable_name] : null;
    }

}

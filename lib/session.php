<?php
class Session{
    static function get($name){
        return (isset($_SESSION[$name])?$_SESSION[$name]:NULL);
    }
    
    static function set($name, $value){
        $_SESSION[$name] = $value;
    }
    
    static function unsetVar($name){
        unset($_SESSION[$name]);
    }    
    
    static function destroy(){
        session_destroy();
    }
    
}
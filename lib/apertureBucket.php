<?php

class apertureBucket {

    private $appRoot;
    private $accessKey = false;
    private $secret = false;
    private $imgProxy = false;
    private $s3 = false;
    private $bucket = false;

    public function __construct($bucket, $public = false) {
        $GLOBALS['aperture']->loadLib("aws/S3.php");

        $this->appRoot = $GLOBALS['aperture']->appRoot;

        $env = $GLOBALS['aperture']->environment;

        $this->accessKey = $env->aws->accessKey;
        $this->secret = $env->aws->secret;

        $this->bucket = $bucket . "." . $env->aws->namespace;

        if (isset($env->aws->imgProxy)) {
            $this->imgProxy = $bucket . "." . $env->aws->imgProxy;
        }

        $this->s3 = new S3($this->accessKey, $this->secret);

        $acl = S3::ACL_PRIVATE;
        if ($public) {
            $acl = S3::ACL_PUBLIC_READ;
        }

        $this->s3->putBucket($this->bucket, $acl);
    }

    public function url($name = false, $https = false) {
        return ($https ? "https://" : "http://") . $this->bucket . ".s3.amazonaws.com" . ($name !== false ? "/" . $name : "");
    }

    public function imgUrl($name = false, $https = false) {
        if ($this->imgProxy !== false) {
            return ($https ? "https://" : "http://") . $this->imgProxy . ($name !== false ? "/" . $name : "");
        } else {
            return $this->url($name, $https);
        }
    }

    public function contents($orderByNewest = true, $prefix = '') {

        $contents = $this->s3->getBucket($this->bucket, $prefix);

        $return = array();
        foreach ($contents as $name => $file) {
            $return[] = $this->processFile($file);
        }
        usort($return, function($a, $b) {
            return $a['meta']['time'] - $b['meta']['time'];
        });

        if ($orderByNewest) {
            $return = array_reverse($return);
        }

        return $return;
    }

    public function get($name) {
        $file = $this->s3->getObjectInfo($this->bucket, $name);

        if (is_array($file)) {
            $file['name'] = $name;
            $file = $this->processFile($file);
        } else {
            $file = false;
        }

        return $file;
    }

    private function processFile($file) {
        $return = false;

        if (is_array($file)) {
            $item = array("meta" => $file);

            $nameParts = explode("-", $item['meta']['name']);
            unset($nameParts[0]);
            $originalName = implode("-", $nameParts);

            $item['name'] = $originalName;

            $item['image'] = $this->isImage($item['meta']['name']);
            $item['src'] = $this->url($item['meta']['name']);

            if ($item['image']) {
                $item['src'] = $this->imgUrl($item['meta']['name']);
            }

            $return = $item;
        }

        return $return;
    }

    public function addFolder($name, $public = true) {
        $acl = S3::ACL_PRIVATE;
        if ($public) {
            $acl = S3::ACL_PUBLIC_READ;
        }
        return $this->s3->putObjectString($name, $this->bucket, $name, $acl);
    }

    public function add($file, $prefix = '', $name = false, $public = true) {
        $acl = S3::ACL_PRIVATE;
        if ($public) {
            $acl = S3::ACL_PUBLIC_READ;
        }

        $continue = true;

        if (!is_array($file)) {
            $root = $file;

            if (file_exists($root)) {
                $file = $this->s3->inputFile($root);

                if ($name === false) {
                    $name = basename($root);
                }
            } else {
                $continue = false;
            }
        } else {
            if (isset($file['tmp_name']) && isset($file['name'])) {
                if ($name === false) {
                    $name = $file['name'];
                }

                $file = $this->s3->inputFile($file['tmp_name']);
            } else {
                $continue = false;
            }
        }

        if ($continue) {
            $name = ($prefix ? $prefix . '_' : '') . $this->uniqueName($name);
            $upload = $this->s3->putObject($file, $this->bucket, $name, $acl);

            if ($upload) {
                return $this->get($name);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function remove($name) {
        return $this->s3->deleteObject($this->bucket, $name);
    }

    public function rename($name, $newname) {
        $prefix = '';
        $explodename = explode("-", $name);
        $prefixname = explode("_", $explodename[0]);
        if (count($prefixname) > 1) {
            $prefix = $prefixname[0].'_';
        }
        $newname = $prefix.$this->uniqueName($newname);
        $result = $this->s3->copyObject($this->bucket, $name, $this->bucket, $newname);
        if ($result) {
            $this->remove($name);
            return $newname;
        }
    }

    private function uniqueName($name) {
        $key = substr(md5(uniqid(mt_rand(), true)), 0, 8);
        return $key . "-" . $name;
    }

    private function isImage($filename) {
        $extensions = array("png", "gif", "jpg", "jpeg");
        $valid = false;

        $fileExtension = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

        foreach ($extensions as $extension) {
            if ($fileExtension == $extension) {
                $valid = true;
            }
        }

        return $valid;
    }

}

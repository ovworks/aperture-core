<?php

class Response {

    static $result = false;
    static $message = "";
    static $data = array();

    static function setAll($result, $message, $data = array()) {
        Response::$result = $result;
        Response::$message = $message;
        Response::$data = $data;
    }

    static function echoResponse() {
        $response = new stdClass;
        $response->result = self::$result;
        $response->message = self::$message;
        $response->data = self::$data;
        
        Utils::dataModelForeachToArray($response->data);
        header('Content-Type: application/json');
        echo json_encode($response, JSON_PRETTY_PRINT);
        die;
    }
    
    static function setAllAndEchoResponse($result, $message, $data = array()) {
        self::setAll($result, $message, $data);
        self::echoResponse();
        die;
    }

    static function addData($name, $data) {
        self::$data[$name] = $data;
    }

    static function addObjectError($name, $object, $full_messages = false) {
        self::$data[$name] = $object->errors->{$full_messages ? 'full_messages' : 'get_raw_errors'}();
    }

    //default responses
    static function noPermission($message = 'No Permission') {
        Response::$result = false;
        Response::$message = $message;
        Response::$data = array();
        self::echoResponse();
        die;
    }

}

<?php
class sitemetas {

    static $instance;
    public $metas = array();

    /** @return self */
    static function start() {
        if(!static::$instance){
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct() {}

    public function getMetas() {
        return $this->metas;
    }
    
    public function add($meta){
        $this->metas[] = $meta;
    }
    
    

}


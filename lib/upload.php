<?php

class upload {

    var $error = false;
    var $files = array();

    public function uploadImages($saveDirectory, $imageName, $size = array(700, 700, 'auto'), $extension = 'jpg') {
        $input = $imageName;
        if(empty($_FILES[$imageName])){
            $input = 'images';
        }
        $this->uploadFiles($input, $saveDirectory);
        $max = 1;
        $dirandname = $saveDirectory.$imageName;
        foreach (glob($dirandname."_*") as $filename) {
            $fileInt =  (int)str_replace($dirandname.'_', '', $filename);
            if ($fileInt >= $max) {
                $max = $fileInt + 1;
            }
        }
        foreach ($this->files as $imgNum => &$imageRoot) {
            $resize = new resize($imageRoot);
            $resize->resizeImage($size[0], $size[1], $size[2]);
            $newImage = $saveDirectory . $imageName . '_' . ($imgNum + $max) . '.' . $extension;
            $resize->saveImage($newImage);
            unlink($imageRoot);
            $imageRoot = $newImage;
        }
    }

    public function uploadFiles($nameFiles, $saveDirectory) {
        if (empty($_FILES[$nameFiles])) {
            $this->error = true;
            return false;
        }
        $files = $_FILES[$nameFiles];
        foreach ($files['name'] as $key => $fileName) {
            if ($files['error'][$key] !== UPLOAD_ERR_OK || !$fileName) {
                $this->error = true;
                continue;
            }
            $file = array();
            $file['name'] = $fileName;
            $file['type'] = $files['type'][$key];
            $file['tmp_name'] = $files['tmp_name'][$key];
            $file['size'] = $files['size'][$key];

            if (!file_exists($saveDirectory)) {
                mkdir($saveDirectory, 0777, true);
            }

            $nameSaveFile = $saveDirectory . uniqid() . '_' . $fileName;
            if (move_uploaded_file($file['tmp_name'], $nameSaveFile)) {
                $this->files[] = $nameSaveFile;
            }
        }
    }

}

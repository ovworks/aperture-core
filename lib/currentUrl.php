<?php

class currentUrl {

    public $url;
    public $getVariables = array();

    static function start($url) {
        $parseurl = parse_url($url);
        $urlNow = $parseurl['scheme'] . '://' . $parseurl['host'] . $_SERVER['REQUEST_URI'];
        return new self($urlNow);
    }

    public function __construct($url) {
        $request = parse_url($url);
        $this->url = $request['scheme'] . '://' . $request['host'] . $request['path'];
        if (isset($request['query'])) {
            parse_str($request['query'], $this->getVariables);
        }
    }

    public function __toString() {
        return $this->url;
    }

    public function set($key, $value) {
        $this->getVariables[$key] = $value;
        return new self($this->get());
    }

    public function get() {
        return $this->url . '?' . http_build_query($this->getVariables);
    }

}

<?php

/*
*   Latch Digital: Google Drive CURL library
*   Allows you to quickly connect to google drive and retrieve a directory
*   Usage: todo
*/

class googleDrive{
    var $accountHosted = true;
    var $accountEmail = "latch@latchdigital.co.nz";
    var $accountPassword = "P2t648Bg2348yMi";
    
    var $curl = false;
    var $auth = false;
    
    function __construct($hosted = "", $email = "", $password = ""){
        if(!empty($hosted))
            $this->accountHosted = $hosted;
        
        if(!empty($email))
            $this->accountEmail = $email;
        
        if(!empty($password))
            $this->accountPassword = $password;
    }
    
    function login(){

        $clientlogin_post = array(
            "accountType" => ($this->accountHosted ? "HOSTED" : "GOOGLE"),
            "Email" => $this->accountEmail,
            "Passwd" => $this->accountPassword,
            "service" => "writely",
            "source" => "latchDigital-latchMedia-01"
        );
        
        $this->curl = curl_init("https://www.google.com/accounts/ClientLogin");
        
        curl_setopt($this->curl, CURLOPT_POST, true);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $clientlogin_post);
        curl_setopt($this->curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        
        $this->auth = $this->authorize();
        
        if($this->auth !== false)
            return true;
        else
            return false;
    }
    
    function authorize(){
        if($this->curl !== false){
            $response = curl_exec($this->curl);
            
            preg_match("/Auth=([a-z0-9_\-]+)/i", $response, $matches);
            
            if(isset($matches[1])){
                $auth = $matches[1];
            }else{
                $auth = false;
            }
            
            return $auth;
        }else{
            return false;
        }
    }
    
    function getDirectory($folderID){
        if($this->auth !== false){
            $headers = array(
                "Authorization: GoogleLogin auth=" . $this->auth,
                "GData-Version: 3.0",
            );
            
            curl_setopt($this->curl, CURLOPT_URL, "https://docs.google.com/feeds/default/private/full/folder%3A" . $folderID . "/contents");
            curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($this->curl, CURLOPT_POST, false);
            
            $response = curl_exec($this->curl);
            
            $return = Utils::toArray(simplexml_load_string($response));
            
            if(!isset($return['entry']))
                $return['entry'] = array();
            
            if(Utils::isAssoc($return['entry']))
                $return['entry'] = array($return['entry']);
            
            foreach($return['entry'] as $i => $document){
                $document['export'] = $document['content']['@attributes']['src'] . "&format=pdf";
                
                if($document['content']['@attributes']['type'] != "text/html"){
                    $id = strstr($document['id'], '%3A');
                    $id = str_replace("%3A", "", $id);
                    $document['export'] = "https://docs.google.com/uc?export=download&id=".$id;
                }
                $return['entry'][$i] = $document;
            }
            
            return $return;
        }else{
            return false;
        }
    }
    
    function close(){
        if($this->curl !== false){
            curl_close($this->curl);
        }
        $this->curl = false;
        $this->auth = false;
    }
}

    
class Utils{
    public static function toArray($object){
        $array = json_decode(json_encode((array) $object), TRUE);

        return $array;
    }
    
    public static function isAssoc($array) {
        if(is_array($array)){
            return (bool)count(array_filter(array_keys($array), 'is_string'));
        }else{
            return false;
        }
    }
}

?>
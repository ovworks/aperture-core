<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Teste
 *
 * @author Stevan
 */
class Utils {

    static function zipData($source, $destination){
        if (extension_loaded('zip')) {
            if (file_exists($source)) {
                $zip = new ZipArchive();
                if ($zip->open($destination, ZIPARCHIVE::CREATE)) {
                    $source = realpath($source);
                    if (is_dir($source)) {
                        $basename = basename($source);
                        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
                        $i = 0;
                        foreach ($files as $file) {
                            if(in_array(basename($file), array('.', '..')))continue;
                            
                            $file = realpath($file);
                            $where = $basename.str_replace($source, '', $file);
                            
                            if (is_dir($file)) {
                                $zip->addEmptyDir($where);
                            } else if (is_file($file)) {
                                $zip->addFromString($where, file_get_contents($file));
                            }
                        }
                    } else if (is_file($source)) {
                        $zip->addFromString(basename($source), file_get_contents($source));
                    }
                }
                return $zip->close();
            }
        }
        return false;
    }
    
    static function dumpMysqlDatabase($path, $mysqlDB, $user, $password = ''){
        $user = '-u'.$user;
        $password = ($password)?' -p'.$password:'';
        
        $max = 0;
        foreach (glob($path."/*.sql") as $file) {
            $name = substr(basename($file), strlen($mysqlDB)+1);
            if ($max <= intval($name)) {
                $max = intval($name);
            }
        }        
        
        $mysqlDumpFile = $path.'/'.$mysqlDB.'_'.sprintf("%03d", ($max+1)).'.sql';
        $mysqlCommand = 'mysqldump '.$user.$password.' '.$mysqlDB.' > '.$mysqlDumpFile;
        exec($mysqlCommand);
        return $mysqlDumpFile;
    }

    static function recurse_copy($src, $dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ( $file = readdir($dir))) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if (is_dir($src . '/' . $file)) {
                    self::recurse_copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    static function getModelAttributes(&$model) {
        $refClass = new ReflectionClass(get_class($model));
        if ($refClass->isSubclassOf("ActiveRecord\\Model")) {
            return $model->attributes();
        } else {
            return (array) $model;
        }
    }

    static function dataModelForeachToArray(&$response) {
        foreach ($response as $key => &$value) {
            if (is_object($value)) {
                $response[$key] = Utils::getModelAttributes($value);
            } else if (is_array($value)) {
                self::dataModelForeachToArray($value);
            }
        }
    }

    static function getElements($elementName, $str) {
        preg_match_all('/\[' . $elementName . '\s*(\w+=".*?")\]/', $str, $matches);
        $attributesMatches = array();
        foreach ($matches[1] as $key => $value) {
            $attributes = array();
            $matchesSplit = preg_split(
                    '/\s*(\w+)="(.*?)\s*"/', $value, NULL, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE
            );
            for ($i = 0; $i < count($matchesSplit); $i+=2) {
                $attributes[$matchesSplit[$i]] = $matchesSplit[$i + 1];
            }
            $attributesMatches[$matches[0][$key]] = $attributes;
        }
        return $attributesMatches;
    }
    
    static function validateUrl($url){
        if(filter_var($url, FILTER_VALIDATE_URL)){
            return $url;
        }
        $url = 'http://'.$url;        
        if(filter_var($url, FILTER_VALIDATE_URL)){
            return $url;
        }
        return false;
    }

}

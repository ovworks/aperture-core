<?php

class loadTime {

    private $time_start = 0;
    private $time_end = 0;
    private $time = 0;
    private $comment = 0;

    public function __construct($comment = false) {
        $this->start($comment);
    }

    public function __destruct() {
        $this->show();
    }
    
    public function start($comment){
        $this->comment = $comment;
        $this->time_start = microtime(true);        
    }

    public function show($Refresh = false){                        
        $this->time_end = microtime(true);
        $this->time = $this->time_end - $this->time_start;
        if($this->comment) echo '<!-- ';
        echo "Loaded in $this->time seconds\n";
        if($this->comment) echo ' -->'."/n ";   
        if($Refresh){
            $this->start($this->comment);
        }     
    }

}

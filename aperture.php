<?php

session_start();

class Aperture {

    public $settings;
    public $app;
    public $views;
    public $environment;
    public $appURL;
    public $appRoot;
    public $inheritRoot;
    public $coreRoot;
    private $configFile;
    private $modelFolder;
    private $viewsFolder;

    public function __construct($inheritRoot = "", $inheritConfig = false, $inheritModel = false, $inheritViews = false) {

        $this->inheritRoot = $inheritRoot;
        $this->coreRoot = "../" . $this->inheritRoot;

        // Load dependencies
        $this->loadDependencies();

        // Load flight
        $this->app = new flight\Engine();

        // Set directory and file roots
        $this->configFile = ($inheritConfig ? $this->appRoot . $inheritRoot . "app/aperture.json" : $this->appRoot . "app/aperture.json");
        $this->modelFolder = ($inheritModel ? $this->appRoot . $inheritRoot . "app/models" : $this->appRoot . "app/models");
        $this->viewsFolder = ($inheritViews ? $this->appRoot . $inheritRoot . "app/views" : $this->appRoot . "app/views");

        // Load settings from config file
        if (file_exists($this->configFile)) {
            $settingsJSON = file_get_contents($this->configFile);

            $this->settings = json_decode($settingsJSON);

            if (empty($this->settings)) {
                $this->app->error(new Exception("Cannot read configuration file '" . $this->configFile . "'. Failed JSON validation."));
            }
        } else {
            $this->app->error(new Exception("Cannot find configuration file '" . $this->configFile . "' in app root."));
        }

        // Set application environment
        $this->environment = $this->getEnvironment();
        if ($this->environment === false) {
            $this->app->error(new Exception("At least one matching environment and domain must be defined in your app configuration file '" . $this->configFile . "'."));
        }

        // Set error reporting level
        $this->app->set('flight.log_errors', true);

        if (!isset($this->environment->errorReporting))
            $this->environment->errorReporting = true;

        if ($this->environment->errorReporting) {
            error_reporting(E_ALL);
        } else {
            $this->app->map('error', function(Exception $ex) {
                die();
            });
            error_reporting(0);
        }

        // Load Models
        $this->models();

        // Load Views
        $this->views();
    }

    private function loadDependencies() {
        $this->appURL = $this->getAppURL();
        $this->appRoot = $this->getAppRoot();

        $this->loadLib("flight/autoload.php");
        $this->loadLib("activeRecord/ActiveRecord.php");
        $this->loadLib("password.php");
        $this->loadLib("session.php");
        $this->loadLib("p.php");
        $this->loadLib("utils.php");

        require_once($this->appRoot . $this->coreRoot . 'aperture-core/base/apertureRoutes.php');
        require_once($this->appRoot . $this->coreRoot . 'aperture-core/base/apertureController.php');
        require_once($this->appRoot . $this->coreRoot . 'aperture-core/base/apertureModel.php');
        require_once($this->appRoot . $this->coreRoot . 'aperture-core/base/apertureEndpoint.php');

        require_once($this->appRoot . 'app/routes.php');
    }

    public function loadLib($name) {
        $appLib = $this->appRoot . $this->inheritRoot . 'app/lib/' . $name;
        $coreLib = $this->appRoot . $this->coreRoot . 'aperture-core/lib/' . $name;

        if (file_exists($appLib)) {
            require_once($appLib);
        } else {
            require_once($coreLib);
        }
    }

    private function registerTwigAutoloader() {
        spl_autoload_register(array($this, 'twigAutoload'));
    }

    private function twigAutoload($class) {
        if (strpos($class, 'Twig') !== false) {

            $file = $this->appRoot . $this->coreRoot . 'aperture-core/lib/' . str_replace(array('_', "\0"), array('/', ''), $class) . '.php';

            if (file_exists($file)) {
                require_once($file);
            }
        }
    }

    private function models() {
        try {
            $environment = $this->environment;
            $modelFolder = $this->modelFolder;
            if (isset($environment->connection)) {
                $connections = array("default" => $environment->connection);
                if (isset($_SESSION['subconnection'])) {
                    $connections['subconnection'] = $environment->connection . '_' . $_SESSION['subconnection'];
                }
                ActiveRecord\Config::initialize(function($cfg) use ($modelFolder, $connections) {
                    $cfg->set_model_directory($modelFolder);
                    $cfg->set_connections($connections);
                    $cfg->set_default_connection('default');
                    // log sql
//                    require_once 'c:\xampp\php\PEAR\Log.php';
//                    $logger = Log::singleton('file', 'phpar.log', 'ident', array('mode' => 0664, 'timeFormat' => '%Y-%m-%d %H:%M:%S'));
//                    $cfg->set_logging(true);
//                    $cfg->set_logger($logger);
                });
            }
        } catch (Exception $e) {
            $this->app->error($e);
        }
    }

    public function views() {
        try {
            $this->registerTwigAutoloader();

            $environment = $this->environment;

            $viewsFolder = $this->viewsFolder;
            $loader = new Twig_Loader_Filesystem($viewsFolder);

            if (!isset($environment->cacheViews))
                $environment->cacheViews = false;

            $this->views = new Twig_Environment($loader, array(
                'cache' => ($environment->cacheViews ? $viewsFolder . '/cache' : false)
            ));
        } catch (Exception $e) {
            $this->app->error($e);
        }
    }

    private function getEnvironment() {
        $environments = $this->settings->environments;
        $url = str_replace(array("http://", "https://"), array("", ""), $this->appURL);

        $environment = false;

        if ((isset($environments)) && (count($environments) > 0)) {
            $i = 0;
            $firstEnv = false;

            foreach ($environments as $env => $envObj) {
                if ($i == 0) {
                    $firstEnv = $envObj;
                    $firstEnv->name = $env;
                }

                if (strpos($url, $envObj->url) === 0) {
                    $environment = $envObj;
                    $environment->name = $env;
                    $environment->wildcard = 0;
                } else if (isset($envObj->wildcards)) {
                    foreach ($envObj->wildcards as $keyWildcard => $wildcard) {
                        if (strpos($url, $wildcard) === 0) {
                            $environment = $envObj;
                            $environment->name = $env;
                            $environment->url = $url;
                            $environment->wildcard = $keyWildcard;
                        }
                    }
                }

                $i = $i + 1;
            }
        }

        return $environment;
    }

    private function getAppURL() {
        $siteurl = 'http';
        if (isset($_SERVER["HTTPS"])) {
            if ($_SERVER["HTTPS"] == "on") {
                $siteurl .= "s";
            }
        }
        $siteurl .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $siteurl .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["SCRIPT_NAME"];
        } else {
            $siteurl .= $_SERVER["SERVER_NAME"] . $_SERVER["SCRIPT_NAME"];
        }
        $find = '/';
        $after_find = substr(strrchr($siteurl, $find), 1);
        $strlen_str = strlen($after_find);
        $siteurl = substr($siteurl, 0, -$strlen_str);

        return $siteurl;
    }

    private function getAppRoot() {
        $siteroot = getcwd();

        $siteroot = realpath($siteroot);
        $siteroot = str_replace('\\', "/", $siteroot) . "/";

        return $siteroot;
    }

}
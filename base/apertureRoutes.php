<?php
    class apertureRoutes{
        var $aperture;
        var $app;
        var $controllers;

        public function __construct($aperture){
            $this->aperture = $aperture;
            $this->app = $this->aperture->app;

            $this->controllers = array();
        }

        public function to($controller, $action){
            $className = $controller;
            $actionName = $action . "Action";

            if(!isset($this->controllers[$className])){
                $this->controllers[$className] = $this->loadController($className);
            }

            $controllers = $this->controllers;
            $parent = $this;

            return function() use ($controllers, $className, $actionName, $parent){
                $args = $parent->cutArray(1, func_get_args());
                return call_user_func_array(array($controllers[$className], $actionName), $args);
            };
        }
        
        public function cutArray($length, $array){
            for($i = 1; $i <= $length; $i++){
                array_pop($array);
            }
            return $array;
        }
        
        public function loadController($name){
            $name = $name . "Controller";
            if(class_exists($name)){
                return new $name;
            }else{
                $classRoot = $this->aperture->appRoot . 'app/controllers/' . $name . '.php';
                if(file_exists($classRoot)){
                    require_once($classRoot);
                    if(class_exists($name)){
                        return new $name;
                    }else{
                        $this->app->error(new Exception("Could not find controller: '" . $name . "'."));
                    }
                }else{
                    $this->app->error(new Exception("Could not find controller file: '" . $classRoot . "'."));
                }
            }
        }
        
        public function _loadLib($name){
            return $GLOBALS['aperture']->loadLib($name);
        }
    }
?>
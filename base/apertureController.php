<?php

class apertureController {

    public $app;
    public $settings;
    public $views;
    public $environment;
    public $appURL;
    public $appRoot;
    public $inheritRoot;
    public $coreRoot;

    public function __construct() {
        $this->app = $GLOBALS['aperture']->app;
        $this->settings = $GLOBALS['aperture']->settings;
        $this->views = $GLOBALS['aperture']->views;

        $this->environment = $GLOBALS['aperture']->environment;

        $this->appURL = $GLOBALS['aperture']->appURL;
        $this->appRoot = $GLOBALS['aperture']->appRoot;
        $this->inheritRoot = $GLOBALS['aperture']->inheritRoot;
        $this->coreRoot = $GLOBALS['aperture']->coreRoot;
    }

    public function _loadLib($name) {
        return $GLOBALS['aperture']->loadLib($name);
    }

    public function _loadModel($name) {
        require_once($this->appRoot . $this->inheritRoot . 'app/models/' . $name . ".php");
    }

    public function _loadEndpoint($name) {
        require_once($this->appRoot . $this->inheritRoot . 'app/endpoints/' . $name . "Endpoint.php");
        $class = $name . "Endpoint";
        return new $class;
    }

    protected function _setViewVars($vars) {
        $vars['_app'] = new stdClass();
        $vars['_app']->url = $this->appURL;
        $vars['_app']->settings = $this->settings;

        return $vars;
    }

    public function _extendViewVars($vars) {
        return $vars;
    }
    
    public function _postViewRender($content, $vars){
        return $content;
    }

    protected function view($view, $vars = array()) {
        $vars = $this->_setViewVars($vars);
        $vars = $this->_extendViewVars($vars);
        $content = $this->views->render($view, $vars);
        //$content =  $this->_toUTF8($content);
        return $this->_postViewRender($content, $vars);
    }
    
    protected function _toUTF8($string){
        return iconv(mb_detect_encoding($string, mb_detect_order(), true), "UTF-8", $string);
    }
    
    protected function _isUTF8($string){
        return (preg_match('!!u', $string) ? true : false);
    }

    public function mail($to, $subject, $view, $data = array(), $sender = false, $attachments = array(), $html = true) {
        $this->_loadLib("PHPMailer/PHPMailerAutoload.php");

        $mail = new PHPMailer();

        if (isset($this->environment->smtp)) {
            $smtp = $this->environment->smtp;
            $mail->isSMTP();
            $mail->Host = $smtp->host;
            if (isset($smtp->port)) {
                $mail->Port = $smtp->port;
            }
            if (isset($smtp->auth)) {
                $mail->SMTPAuth = $smtp->auth;
                $mail->Username = $smtp->username;
                $mail->Password = $smtp->password;
                $mail->SMTPSecure = $smtp->encryption;
            }
        }


        $mail->From = $this->settings->mail->from;
        $mail->FromName = $this->settings->mail->fromName;

        if (is_array($sender)) {
            $mail->From = (isset($sender['from']) ? $sender['from'] : $mail->From);
            $mail->FromName = (isset($sender['fromName']) ? $sender['fromName'] : $mail->FromName);
            if (isset($sender['replyTo'])) {
                $mail->addReplyTo($sender['replyTo'], $mail->FromName);
            }
        }

        $mail->addAddress($to);

        if ($html) {
            $mail->isHTML(true);
        }

        $mail->Subject = $subject;
        $mail->Body = $this->view($view, $data);

        foreach ($attachments as $name => $attachment) {
            $mail->addAttachment($attachment, $name);
        }

        return $mail;
    }

    public function backupAction() {
        $mysqlConnection = substr($this->environment->connection, 8);
        $mysqlConnectionExplode = explode('@', $mysqlConnection, 2);
        $root = explode(':', $mysqlConnectionExplode[0], 2);
        $password = '';
        if (count($root) > 1) {
            $password = $root[1];
        }
        $mysqlDB = explode('/', $mysqlConnectionExplode[1], 2);
        $mysqlDumpFile = Utils::dumpMysqlDatabase($this->appRoot . 'app/sql', $mysqlDB[1], $root[0], $password);
        echo '<pre>';
        die(var_dump($mysqlDumpFile));

        $max = 0;
        foreach (glob($this->appRoot . "$mysqlDB[1]_*.zip") as $file) {
            $name = substr(basename($file), strlen($mysqlDB) + 1);
            if ($max <= intval($name)) {
                $max = intval($name);
            }
        }
        $zipDumpFile = $this->appRoot . $mysqlDB[1] . '_' . sprintf("%03d", ($max + 1)) . '.zip';

        $backup = array($mysqlDumpFile, $this->appRoot . 'assets/uploads/');
        foreach ($backup as $fileFolder) {
            Utils::zipData($fileFolder, $zipDumpFile);
        }
        unlink($mysqlDumpFile);
    }

}

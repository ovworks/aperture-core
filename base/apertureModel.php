<?php

class apertureModel extends ActiveRecord\Model {

    public static function _appURL() {
        return $GLOBALS['aperture']->appURL;
    }

    public static function _appRoot() {
        return $GLOBALS['aperture']->appRoot;
    }

    public static function _inheritRoot() {
        return $GLOBALS['aperture']->inheritRoot;
    }

    public static function _coreRoot() {
        return $GLOBALS['aperture']->coreRoot;
    }

    public static function _settings() {
        return $GLOBALS['aperture']->settings;
    }

    public static function _environment() {
        return $GLOBALS['aperture']->environment;
    }

    public static function _loadLib($name) {
        return $GLOBALS['aperture']->loadLib($name);
    }

    public static function _loadEndpoint($name) {
        require_once(self::_appRoot() . self::_inheritRoot() . 'app/endpoints/' . $name . "Endpoint.php");
    }

    public function passedValidation() {
        return (count($this->errors->full_messages() < 1) ? true : false);
    }

    /**
     * @param array $postVariables
     */
    public function savePost($postVariables = array()) {
        $this->getMultiplePostData($postVariables);
        $this->save();
    }
    
    public function getMultiplePostData($postVariables){        
        foreach ($postVariables as $key => $variable) {
            $this->getPostData($key, $variable);
        }
    }
    
    /**
     * @param string $key //data column
     * @param array $variable //value = preDefine //variablename = postVariable //rule = ragex
     */
    public function getPostData($key, $variable = false) {
        if (is_array($variable)) {
            if (isset($variable['value'])) {
                $this->$key = $variable['value'];
            } else {
                $variablename = isset($variable['variablename']) ? $variable['variablename'] : $key;
                $filter = isset($variable['filter']) ? $variable['filter'] : FILTER_DEFAULT;
                $this->$key = filter_input(INPUT_POST, $variablename, $filter);
                //validation
                if (!empty($variable['rule']) && !preg_match($variable['rule'], $this->$key)){
                    return array($variablename, ' is not valid.');
                }
            }
        } else {
            $this->$variable = filter_input(INPUT_POST, $variable, FILTER_DEFAULT);
        }
    }
    
    /**
     * 
     */
    static function generateShortname($name, $columnShortname = 'shortname') {
        $findBy = 'find_by_'.$columnShortname;
        $shortname = preg_replace("/[^a-zA-Z0-9 \-\_]/", "", $name);
        $shortname = strtolower($shortname);
        $finalShortName = $shortname = str_replace(" ", "-", $shortname);  
        $i = 0;
        while (self::$findBy($finalShortName)){            
            $finalShortName = $shortname.'-'.(++$i);
        }
        return $finalShortName;
    }
    
    public static function generateToken(){
        return substr(md5(uniqid(rand(), true)), 16, 16);
    }
    
    public static function processBase64Image($imageString){
        $return = false;
        $imageStringParts = explode(",", $imageString);
        if(count($imageStringParts) > 1){
            $base64 = trim($imageStringParts[1]);
            if(trim($imageStringParts[0]) == "data:image/png;base64"){
                $img = imagecreatefromstring(base64_decode($base64));
                if($img){
                    $imgName = self::generateToken() . ".png";
                    $tempDir = self::_appRoot() . "app/temp/";

                    $imgDir = $tempDir . $imgName;

                    if(imagepng($img, $imgDir)){
                        $return = $imgDir;
                    }
                }
            }
        }
        
        return $return;
    }

    public static function getBase64Image($url){
        $string = 'data:image/' . pathinfo($url, PATHINFO_EXTENSION) . ';base64,';
        $filter_action = "php://filter/read=convert.base64-encode/resource=" . $url;
        $string .= file_get_contents($filter_action);
        return $string;
    }
    
    public static function clearTempFile($file){
        $return = false;
        
        
        
        if(file_exists($file)){
            if(unlink($file)){
                $return = true;
            }
        }
        
        return $return;
    }

    public static function validateImageFile($imageFile, $maxSize = 4){
        $return = false;
        
        if(file_exists($imageFile)){
            $info = getimagesize($imageFile);
            $filesize = number_format(filesize($imageFile)  / 1048576, 1);
            
            if($filesize <= $maxSize){
                if ($info[0] > 0 && $info[1] > 0 && $info['mime']) {
                    $return = true;
                }
            }
        }
        
        return $return;
    }

}

<?php

class apertureEndpoint {

    public $resource = "";
    public $app;
    public $settings;
    public $views;
    public $environment;
    public $appURL;
    public $appRoot;
    public $inheritRoot;
    public $coreRoot;
    public $endpoint = "";
    private $result = false;

    public function __construct() {
        $this->app = $GLOBALS['aperture']->app;
        $this->settings = $GLOBALS['aperture']->settings;
        $this->views = $GLOBALS['aperture']->views;

        $this->environment = $GLOBALS['aperture']->environment;

        $this->appURL = $GLOBALS['aperture']->appURL;
        $this->appRoot = $GLOBALS['aperture']->appRoot;
        $this->inheritRoot = $GLOBALS['aperture']->inheritRoot;
        $this->coreRoot = $GLOBALS['aperture']->coreRoot;

        $this->endpoint = $this->environment->endpoint->domain . $this->resource;

        $this->requestContent();
    }

    public function file_get_contents($request) {
        $md5Name = $this->appRoot . $this->inheritRoot . 'app/endpoints/cache/' . md5($request) . '.txt';

        $cache = false;

        if (isset($this->environment->endpoint->cache)) {
            if ($this->environment->endpoint->cache == 1) {
                $cache = true;
            }
        }

        if ($cache) {
            $updateCache = false;

            if (!is_file($md5Name)) {
                $updateCache = true;
            } elseif ($this->isOld($md5Name)) {
                $updateCache = true;
            }

            if ($updateCache) {
                $result = $this->get_url($request);
                file_put_contents($md5Name, $result);
            } else {
                $result = file_get_contents($md5Name);
            }
        } else {
            $result = $this->get_url($request);
        }
        return $result;
    }

    public function get_url($url) {
        //$time = time();
        $tmp = MyCurl::getUrl($url);
        //echo time() - $time . "\n";
        return $tmp;
    }

    private function isOld($cacheFile) {
        if (!isset($this->environment->endpoint->cacheDomain)) {
            return false;
        }

        $testerUrl = $this->environment->endpoint->cacheDomain . $this->resource;
        $testerResponse = json_decode($this->get_url($testerUrl));

        $lastPublishedTime = false;
        if (isset($testerResponse->LastPublished->LastPublishedDate)) {
            $lastPublishedTime = strtotime($testerResponse->LastPublished->LastPublishedDate);
        }

        if ($lastPublishedTime !== false) {
            $preDates = $this->appRoot . $this->inheritRoot . 'app/endpoints/cache/preDates.json';
            $preDatesJson = new stdClass;
            if (is_file($preDates)) {
                $preDatesJson = json_decode(file_get_contents($preDates));
            }

            $lastCacheTime = isset($preDatesJson->{basename($cacheFile)}) ? $preDatesJson->{basename($cacheFile)} : 0;

            if ($lastPublishedTime > $lastCacheTime) {
                $preDatesJson->{basename($cacheFile)} = $lastPublishedTime;
                file_put_contents($preDates, json_encode($preDatesJson));
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function requestContent() {
        $request = $this->file_get_contents($this->endpoint);
        $this->result = json_decode($request, true);
    }

    public function _loadLib($name) {
        return $GLOBALS['aperture']->loadLib($name);
    }

    public function _loadModel($name) {
        require_once($this->appRoot . $this->inheritRoot . 'app/models/' . $name . ".php");
    }

    public function _loadEndpoint($name) {
        require_once($this->appRoot . $this->inheritRoot . 'app/endpoints/' . $name . "Endpoint.php");
    }

    public function get($object = false) {
        if ($this->result === false) {
            return false;
        } else {
            if (($object === false) || (empty($object))) {
                return $this->asObject($this->result);
            } else {
                if (isset($this->result[$object])) {
                    return $this->asObject($this->result[$object]);
                } else {
                    return false;
                }
            }
        }
    }

    private function asObject($array) {
        return json_decode(json_encode($array), FALSE);
    }

}

class MyCurl {

    private static $instance = false;
    private $ch;

    private function __construct() {
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    }

    function __destruct() {
        curl_close(self::instance()->ch);
    }

    static function instance() {
        if (!self::$instance) {
            self::$instance = new MyCurl();
        }
        return self::$instance;
    }

    static function getUrl($url) {
        curl_setopt(self::instance()->ch, CURLOPT_URL, $url);
        return curl_exec(self::instance()->ch);
    }

    static function sendPost($url, $post) {
        $c = &self::instance()->ch;
        //
        curl_setopt($c, CURLOPT_POST, true);
        curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($post));
        //
        curl_setopt($c, CURLOPT_URL, $url);
        return curl_exec($c);
    }

}
